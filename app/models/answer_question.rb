# == Schema Information
#
# Table name: answer_questions
#
#  id          :integer          not null, primary key
#  answer_id   :integer
#  question_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_answer_questions_on_answer_id    (answer_id)
#  index_answer_questions_on_question_id  (question_id)
#
# Foreign Keys
#
#  fk_rails_7f56ae34fe  (answer_id => answers.id)
#  fk_rails_8ece0ea7f6  (question_id => questions.id)
#

class AnswerQuestion < ApplicationRecord
  belongs_to :answer
  belongs_to :question
  belongs_to :user
end
