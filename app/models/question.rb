# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  quiz_id    :integer
#

class Question < ApplicationRecord
  has_many :answers
  has_many :answer_question, through: :answers
  accepts_nested_attributes_for :answers, reject_if: :all_blank, allow_destroy: true

  def check_answer(answer_id)
    answers.where(correct: true, id: answer_id).any?
  end
end
