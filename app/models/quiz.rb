# == Schema Information
#
# Table name: quizzes
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Quiz < ApplicationRecord
  has_many :questions
  has_many :answer_questions, through: :questions
  accepts_nested_attributes_for :questions
  accepts_nested_attributes_for :answer_questions
end
