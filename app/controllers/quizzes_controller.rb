class QuizzesController < ApplicationController
  before_action :authenticate_user!, only: :play
  before_action :all_quiz, only: [:index]
  before_action :find_quiz, only: [:show, :edit, :update, :destroy]

  def new
    @quiz = Quiz.new
    @quiz.questions.build
  end

  def create
    @quiz = Quiz.create(quiz_params)

    respond_to do |format|
      if @quiz.save
        format.html { redirect_to root_path }
        format.json { render :show, status: :created, location: @quiz }
      else
        format.html { render :new }
        format.json { render json: @quiz.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @quiz_question = @quiz.questions.order("RANDOM()").all
  end

  def play
    @quiz = Quiz.find(params[:quiz_id])
    @quiz_question = @quiz.questions.order("RANDOM()").all
  end

  def result
    @quiz = Quiz.find(params[:quiz_id])
    @cq = @quiz.questions
    @count_correct_answer = 0
    @cq.each do |q|
      if q.answers.where(correct: true).ids[0].to_i == params[quiz_result_path]['answer_question_ids'][0].to_i
        @count_correct_answer += 1
      end
    end
  end

  def update
    @quiz.update_attributes(quiz_params)
    redirect_to @quiz
  end

  def destroy
    @quiz.destroy
    redirect_to @quiz
  end

  def answer_question
    @quiz.update(answer_question_params)
  end

  private

  def answer_question_params
    params.require(:quiz).permit(answer_questions_attributes: %i(question_id answer_id user_id))
    layout :false
  end

  def all_quiz
    @quizzes = Quiz.all
  end

  def find_quiz
    @quiz = Quiz.find(params[:id])
  end

  def quiz_params
    params.require(:quiz).permit(:title, questions_attributes: [:id, :title, :_destroy, answers_attributes: [:id, :title, :correct, :_destroy]])
  end
end
