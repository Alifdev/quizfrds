class CreateAnswerQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :answer_questions do |t|
      t.references :answer, foreign_key: true
      t.references :question, foreign_key: true

      t.timestamps
    end
  end
end
