Rails.application.routes.draw do
  devise_for :users
  root 'quizzes#index'

  resources :quizzes do
    get 'result'
    get 'play'
  end
  resources :questions

  patch 'quizzes_answer_question', to: 'quizzes#answer_question'

end
